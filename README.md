# Gibi - the Shadok converter


## Introduction

Gibis, are the opposite to the Shadoks: they are intelligent but vulnerable and
also inhabit a two-dimensional planet. Also gibi in Esperanto means 'gybe', that
is "To shift a fore-and-aft sail from one side of a sailing vessel to the other,
while sailing before the wind." But none of that gives a clue about what this
project is.

This is a set of tools written in [Ruby](https://www.ruby-lang.org) that allows
to convert integer number into their [Shadok](https://fr.wikipedia.org/wiki/Les_Shadoks#Arithm%C3%A9tique_%E2%80%93_compter_en_Shadok).
representation counterpart.

## Level 1

Contains a library that makes the bulk of the work, and a standalone executable.


## Level 2

Contains a [Ruby on Rails](https://rubyonrails.org/) application that use the
above mentioned librabry to provide a conversion service.
