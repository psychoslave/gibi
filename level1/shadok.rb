#!/usr/bin ruby
# Provide facility to convert integers into their shadok numbering equivalents

# Gather elements specific to the shadok language
module Shadok
  # list of endogen digits
  Digits = %w{Ga Bu Zo Meu}
end

class Integer
  # Retrun string representation of the number as expressed in Shadok
  #
  # That is, in base 4 using `shadok_digits`
  def shadok
    self.
      # convert to base 4 string representation
      to_s(4). 
      # use shadok digits and drop first '-' which is supernumerary
      gsub(/[0-4]/){|ordinal| '-' << Shadok::Digits[ordinal.to_i] }[1..-1] 
  end
end
