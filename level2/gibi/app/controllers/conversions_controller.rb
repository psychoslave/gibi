require_relative '../../../../level1/shadok'
class ConversionsController < ApplicationController
  # Render form to create a request
  def new
  end

  # Render response on a conversion to shadok number
  def create
    value = Integer(conversion_params) rescue nil
    if !value.is_a? Integer
      render plain: params[:number].merge({error: 'not a number'}).to_json,
        status: :bad_request
      return
    end
    render plain: params[:number].merge({shadok: value.shadok}).to_json
  end

  private
  def conversion_params
    params.require(:number).require(:value)
  end
end
