Rails.application.routes.draw do
  get 'conversions/new'
 
  root 'conversions#new'
  resources :conversions
end
